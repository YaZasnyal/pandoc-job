#!/bin/sh

WORKING_DIR=$(pwd)

# clone 
if [ ! -d "pandoc-job" ] 
then
    git clone https://gitlab.com/YaZasnyal/pandoc-job.git
fi

# pull
cd pandoc-job 
if [ -d ".git" ] 
then
    git pull
fi
cd $WORKING_DIR

# compile all plantuml files
find . -name *.puml | xargs -I FILE java -jar /root/plantuml.jar -tsvg FILE

/usr/local/bin/pandoc -d pandoc-job/template/defaults.yml --verbose -s $1 -o $(dirname $1)/$(basename $1 .md).pdf

#!/usr/bin/python3
import os
import requests
import argparse
import json
import yaml

parser = argparse.ArgumentParser(prog='document_uploader', description='uploads file to the DMS', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--url', metavar='url', type=str, help="DMS upload endpoint")
parser.add_argument('--src', metavar='source', type=str, help="source file")
parser.add_argument('--pdf', metavar='pdf', type=str, help="copiled PDF")
args = parser.parse_args()

yamlData = None
with open(args.src, mode="r", encoding="utf-8") as f:
    try:
        data = f.read()
        data = data[0:data.find('\n...')]
        yamlData = yaml.load(data, Loader=yaml.Loader)
    except yaml.YAMLError as exc:
        print('Unable to parse yaml: {}', exc)
        exit(-1)

if 'title' not in yamlData:
    print('There is no title in the document')
    exit(-3)

documentMetadata = {
    'docCode': yamlData['doc-code'],
    'docTitle': yamlData['title'],
    'docUid': os.environ['CI_PIPELINE_ID'],
    'docAuthors': yamlData['author'] if 'author' in yamlData else [],

    'gitGroup': os.environ['CI_PROJECT_NAMESPACE'],
    'gitRepo': os.environ['CI_PROJECT_NAME'],
    'gitBranch': os.environ['CI_COMMIT_BRANCH'],
    'gitDefaultBranch': os.environ['CI_DEFAULT_BRANCH'],
    'gitCommit': os.environ['CI_COMMIT_SHA'],
    'gitAuthor': os.environ['CI_COMMIT_AUTHOR'],
    'gitDateTime': os.environ['CI_COMMIT_TIMESTAMP'],
    'gitData': dict(**os.environ),
}

form_data = {
    'metadata': ('metadata.json', json.dumps(documentMetadata), 'application/json'),
    'document': ('file.pdf', open(args.pdf, 'rb'), 'application/pdf')
}

response = requests.post(args.url, files=form_data)
if (response.status_code == 200):
    exit(0)
else:
    exit(response.status_code)

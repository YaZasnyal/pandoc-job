# pandoc-job

Использование:
```yml
include: 
    remote: "https://gitlab.com/YaZasnyal/pandoc-job/-/raw/master/build-tex.yml"

build-tex:
  extends: .build-tex
  variables:
    file: test.md
```

Собрать документ вручную:
```sh
pandoc -d pandoc-job/template/defaults.yml -s document.md -o document.pdf
```

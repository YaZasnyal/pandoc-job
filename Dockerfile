FROM pandoc/latex:2.18-alpine

RUN apk add git &&\
    apk --no-cache add msttcorefonts-installer fontconfig && \
    update-ms-fonts && \
    fc-cache -f

RUN tlmgr update --self && \
    tlmgr install \
        latexmk \
        cm-super koma-script \
        unfonts-extra \
        babel-belarusian babel-bulgarian babel-russian babel-serbian babel-serbianc babel-ukrainian churchslavonic cmcyr cyrillic cyrillic-bin cyrplain disser eskd eskdx gost hyphen-belarusian hyphen-bulgarian hyphen-churchslavonic hyphen-mongolian hyphen-russian hyphen-serbian hyphen-ukrainian lcyw lh lhcyr lshort-bulgarian lshort-mongol lshort-russian lshort-ukr mongolian-babel montex mpman-ru numnameru pst-eucl-translation-bg ruhyphen russ serbian-apostrophe serbian-date-lat serbian-def-cyr serbian-lig t2 texlive-ru texlive-sr ukrhyph xecyrmongolian \
        adjustbox background bidi csquotes footmisc footnotebackref fvextra mdframed pagecolor sourcecodepro sourcesanspro titling ulem upquote xurl \
        letltxmacro zref everypage framed collectbox \
        xecjk filehook unicode-math ucharcat babel-german ly1 mweights needspace \
        awesomebox fontawesome5 \
        tcolorbox pgf etoolbox environ trimspaces changepage  \
        lipsum

RUN apk add --update --no-cache python3 &&\
    python3 -m ensurepip &&\
    pip3 install --no-cache --upgrade pip setuptools pandoc-latex-environment

RUN apk add --update --no-cache openjdk11 graphviz &&\
    wget -O /root/plantuml.jar https://github.com/plantuml/plantuml/releases/download/v1.2022.1/plantuml-1.2022.1.jar

COPY scripts/build_document.sh /build_document.sh

ENTRYPOINT [ "/build_document.sh" ]
